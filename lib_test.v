module main

fn test_translate() {
	assert translate([
		'#!/bin/bash',
		'',
		'mv test1 test2',
		'chmod 777 test2/file',
		'chmod_example 777 test2/file',
		'echo test test',
		'echo_errors',
	]) == [
		'#!/usr/bin/env -S v run',
		'',
		"system('mv test1 test2')",
		"chmod('test2/file', 777)",
		"system('chmod_example 777 test2/file')",
		"println('test test')",
		"system('echo_errors')",
	]
}
